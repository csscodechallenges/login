
/*----------------- Route Configs ------------------ */
Router.configure({
    layoutTemplate: "ApplicationLayout" 
} );

Router.onBeforeAction(
    function() {
        if (!Meteor.user()) {
            Router.go('/');
        }
        this.next();
    }, {except: ['signup']}
);

Accounts.onLogin(function() {
    Router.go('/loggedin');
    this.stop();
});

Accounts.onLoginFailure(function(){
    Router.go('/');
    this.stop();
});

/*----------------- Routes -------------------------- */
Router.route('/', function() {
    this.render("empty", {to: "navbar"});
    this.render("login", {to: "body"});
    this.render("empty", {to: "footer"});
});

Router.route('/loggedin', function() {
    this.render("navbar", {to: "navbar"});
    this.render("loggedin", {to: "body"});
    this.render("footer", {to: "footer"});
})

Router.route('/signup', function() {
    this.render("empty", {to: "navbar"});
    this.render("register", {to: "body"});
    this.render("empty", {to: "footer"});
});

/*------------- NavBar --------------------------------- */
Template.navbar.onRendered(function () {
    $(".dropdown-button").dropdown({ hover: false });
});    
