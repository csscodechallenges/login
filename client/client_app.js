
Template.login.events({
    'submit form': function(event, template) {
        event.preventDefault();
        var username_email = event.target.username_email.value;
        var password = event.target.password.value;
            Meteor.loginWithPassword(username_email, password, function(error) {
                if(error){
                  //  console.log(template.find('#form-messages').html("dfgsdfa"));
                    $(template.find('#form-messages')).html("There was an error logging in: <strong>" + error.reason + "</strong>");
                }
            });
        console.log(username_email);
        
        return;
    }
});

Template.register.events({
    'submit form': function(event, template){
        event.preventDefault();
        var emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var email = event.target.email.value;
        if(!emailRegex.test(email)){
            $(template.find('#form-messages')).html("There was an error: <strong>Please check your email</strong>");
            return;
        }
        var username = event.target.username.value;
        var password = event.target.password.value;
        console.log("Form submitted.");
        Accounts.createUser({
            email: email,
            username: username,
            password: password
        });
        return;
    }
});

Template.navbar.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout(function(){
            Router.go("/");
        });
        return;
    }
});

